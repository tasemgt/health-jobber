import { Pipe, PipeTransform } from '@angular/core';
import { Job } from '../../models/job';

@Pipe({
  name: 'filterJobs',
})
export class FilterUserAppliedJobsPipe implements PipeTransform {


  transform(jobs: Job[], title: string) {
    if(!jobs || !title){
      return jobs;
    }
    return jobs.filter(job => job.job_title.indexOf(title) !== -1);
  }

}
