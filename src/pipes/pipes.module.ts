import { NgModule } from '@angular/core';
import { SanitizerPipe } from './sanitizer/sanitizer';
import { FilterUserAppliedJobsPipe } from './filter-user-applied-jobs/filter-user-applied-jobs';
@NgModule({
	declarations: [SanitizerPipe,
    FilterUserAppliedJobsPipe],
	imports: [],
	exports: [SanitizerPipe,
    FilterUserAppliedJobsPipe]
})
export class PipesModule {}
