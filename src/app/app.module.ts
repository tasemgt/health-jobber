import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from "@ionic-native/background-mode";
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';

import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from "@angular/forms";
import { SwingModule } from 'angular2-swing';
import { DataProvider } from '../providers/data/data';
import { GiphyProvider } from '../providers/giphy/giphy';

import { UserProfilePopover } from '../pages/user-details/user-details';
import { ChatProvider } from '../providers/chat/chat';

import { AuthService } from "./../providers/auth/auth.service";
import { UtilService } from '../providers/util/util.service';
import { JobService } from '../providers/job/job.service';
import { UserService } from '../providers/user/user.service';
import { FirebaseService } from '../providers/util/firebase.service';
import { NotificationProvider } from '../providers/notification/notification';
import { SmartCvProvider } from '../providers/smart-cv/smart-cv';
import { UpdateProvider } from '../providers/update/update';

@NgModule({
  declarations: [MyApp, UserProfilePopover],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SwingModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, { scrollAssist: false }),
    IonicStorageModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, UserProfilePopover],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LocalNotifications,
    BackgroundMode,
    DataProvider,
    GiphyProvider,
    ChatProvider,
    AuthService,
    UtilService,
    JobService,
    UserService,
    FirebaseService,
    SmartCvProvider,
    UpdateProvider,
    NotificationProvider,
  ]
})
export class AppModule {}
