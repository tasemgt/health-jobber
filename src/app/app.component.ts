import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Storage } from '@ionic/storage';

import * as firebase from 'firebase';

import { FIREBASE_CONFIG } from "./app.firebase.config";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: string;

  constructor(
    private platform: Platform,
    private storage: Storage,
    statusBar: StatusBar,
    splashScreen: SplashScreen)
    {


    this.storage.get('currentUser').then(
      (user) =>{
        user? this.rootPage = 'HomePage' : this.rootPage = 'LandingPage'
      }
    ).catch(error => console.log(error));

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

     // this.backgroundMode.on('activate').subscribe(() =>{

      // setInterval(
      //   () =>{
      //     this.scheduleNotification("Sending some background notifications", "Message", new Date(new Date().getTime() + 1000));
      //   }, 5000
      // )
    //  });

      //this.backgroundMode.enable();

      if (this.platform.is('ios') || this.platform.is('android')) {
        //Keyboard.disableScroll(true);
      }
    });

    //Firebase Initialization..
    firebase.initializeApp(FIREBASE_CONFIG);
  }

}
