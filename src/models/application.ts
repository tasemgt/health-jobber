export class Application{
  id: number;
  job_id: number;
  user_id: number;
  job_title: string;
  user_name: string;
  cover_letter: string;
  cv: string;
  created_at: Date;
  updated_at: Date;
  currriculum_vitae: {
    url: string;
  };
  approved: boolean;
}
