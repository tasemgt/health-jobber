export class Job{
  id: number;
  user_id: number;
  job_category: string;
  job_title: string;
  company_name: string;
  location: string;
  job_description: string;
  monthly_salary: string;
  deadline: string;
  contact: string;
  longitude: string;
  lattitude: string;
  fulltime: Date;
  created_at: Date;
  updated_at: Date;
  status: string;
  expiry_date: Date;
  rating: any;
  photo_url: string;
  image: {
    url: string;
    thumb: {
      url: string;
    },
    medium:{
      url: string;
    }
  };
}
