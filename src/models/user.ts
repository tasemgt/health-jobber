import { Application } from './application';
import { Job } from './job';

//Main User Model structure
export class User {
  id: number;
  provider: string;
  uid: string;
  email: string;
  allow_password_change: boolean;
  created_at: Date;
  updated_at: Date;
  name: string;
  role: string;
  address: string;
  phone: string;
  gender: string;
  dob: string;
  day?: string;
  month?: string;
  year?: string;
  country: string;
  state: string;
  company_name: string;
  website: string;
  longitude: string;
  lattitude: string;
  about_company: string;
  avatar: string;
  verified: string;
  edited: string;
  subscribed: string;
  cv: string;
  about: string;
  photo: {
    url: string,
    thumb: {
      url: string
    }
  };
  jobs: Job[];
  applications: Application[];
}


//Signup User Model Structure
export class SignUpCredentials{
  role: string;
  name: string;
  email: string;
  password: string;
}

//Login Credentials User Model Structure
export class Credentials{
  email: string;
  password: string;
}


export class WorkCredentials{
  user_id: number;
  job_title: string;
  company: string;
  responsibilities: string;
  start_date: string;
  end_date: string;

}

export class ProfileDetails{
  user_id: number;
  name?: string;
  about?: string;
  dob?: string;
  address?: string;
  gender?: string;
  phone?: string;
  country?: string;
  state?: string;
}

