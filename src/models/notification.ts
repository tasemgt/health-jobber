export class Notification{

  id: number;
  employer: string;
  job_title: string;
  read: boolean;

}
