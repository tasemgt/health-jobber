export class Chat {
    id?: number;
    senderId: number;
    receiverId: number;
    senderName: string;
    receiverName: string;
    message: string;
    image?: string;
    timestamp: string;
}
