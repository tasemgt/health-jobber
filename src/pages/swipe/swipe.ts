import { UserService } from './../../providers/user/user.service';
import { Direction, StackConfig, SwingStackComponent, SwingCardComponent } from 'angular2-swing';
import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { IonicPage, ModalController, NavParams, Modal } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { JobService } from './../../providers/job/job.service';
import { UtilService } from '../../providers/util/util.service';

import { User } from '../../models/user';

import { Job } from "./../../models/job";

@IonicPage()
@Component({
  selector: "page-swipe",
  templateUrl: "swipe.html"
})
export class SwipePage {
  @ViewChild("myswing1") swingStack: SwingStackComponent;
  @ViewChildren("mycards1") swingCards: QueryList<SwingCardComponent>;

  // Array list of users
  users: Array<User> = [];
  jobs: Job[] = [];
  jobseekers: User[] = [];

  //Helps to keep track of when to show loading or a no more jobs message
  initialJobSearch: boolean;
  initialUserSearch: boolean;

  animateButton: boolean = false;

  // Swing Card Configuration
  stackConfig: StackConfig;

  //Current User
  currentUser: User;

  role: string;

  // Recent Swiped Job
  swipedJob: Job;

  // Recent Swiped Jobseeker
  swipedJobseeker: User;

  editModal: Modal;


  constructor(
    private navParams: NavParams,
    private jobService: JobService,
    private userService: UserService,
    private storage: Storage,
    private modalCtrl: ModalController,
    private utilService: UtilService
  ) {}

  ngOnInit() {
    this.cardConfig();
    this.currentUser = this.navParams.get("user");
    if(!this.currentUser){
      this.storage.get("currentUser").then(user => {
        this.currentUser = JSON.parse(user).data;
        if (this.currentUser.role === "jobseeker") {
          this.role = "jobseeker";
          this.getJobList();
        } else {
          this.role = "employer";
          this.getJobSeekers();
        }
        if (!this.currentUser.edited) {
          this.editModal = this.loadEditModal();
        }
      });
    }
    //this.getUserList();
  }

  //Every snap at swipe page fetch avail job list
  ionViewWillEnter() {
    console.log('I entered...');
    // this.storage.get("currentUser").then(user => {
    //   this.currentUser = JSON.parse(user).data;
    //   if (this.currentUser.role === "jobseeker") {
    //     this.role = 'jobseeker'
    //     this.getJobList();
    //   } else {
    //     this.role = 'employer'
    //     this.getJobSeekers();
    //   }
    // });
  }

  //Fetch the list of jobs
  getJobList(): void {
    this.initialJobSearch = true;
    this.jobService.getJobList().subscribe(
      (response: Job[]) => {
        this.jobs = response;
        console.log(this.jobs);
      },
      (error: HttpErrorResponse) => {
        console.error(error.message);
        this.utilService.showToast(
          "No internet connection..."
        );
        // setTimeout(() => {
        //   this.getJobList();
        // }, 10000);
      }
    );
  }

  //Fetch the list of Job seekers
  getJobSeekers(): void {
    this.initialUserSearch =  true;
    this.userService.getJobSeekers().subscribe(
      (response: User[]) => {
        this.jobseekers = response;
        console.log(this.jobseekers);
      },
      (error: HttpErrorResponse) => {
        console.error(error.message);
      }
    );
  }

  //Set card stack configuration
  cardConfig() {
    this.stackConfig = {
      allowedDirections: [
        Direction.UP,
        Direction.DOWN,
        Direction.LEFT,
        Direction.RIGHT
      ],
      throwOutConfidence: (offsetX, offsetY, element) => {
        return Math.min(
          Math.max(
            Math.abs(offsetX) / (element.offsetWidth / 2),
            Math.abs(offsetY) / (element.offsetHeight / 2)
          ),
          1
        );
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },
      throwOutDistance: d => {
        return 800;
      }
    };
  }

  //Called whenever we drag an element
  onItemMove(element, x, y, r) {
    var color = "";
    // var abs = Math.abs(x);
    // let min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));
    element.style.background = color;
    element.style[
      "transform"
    ] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }

  //Removes job from card stack and sets status to either a like or dislike.
  public like(status: boolean) {

    this.animateButton = true;

    if(this.role === 'jobseeker'){
      // Currently removed job from card
      this.initialJobSearch = false;
      let removedJob = this.jobs.pop();

      // If status is true, current user likes swiped Job user otherwise a dislike
      if (status) {
        this.swipedJob = removedJob;
        if (!this.currentUser) {
          this.storage.get("currentUser").then(user => {
            this.currentUser = JSON.parse(user).data;
            console.log(!this.currentUser.edited);
            if(!this.currentUser.edited){
              return this.utilService.showToast('Please update your profile to apply for a job');
            }
            this.applyForJob(this.swipedJob.id, this.currentUser.id, this.swipedJob.job_title, this.currentUser.name);
          });
        } else {
          if(!this.currentUser.edited){
              return this.utilService.showToast('Please update your profile to apply for a job');
            }
          this.applyForJob(this.swipedJob.id, this.currentUser.id, this.swipedJob.job_title, this.currentUser.name);
        }
        //this.goToMatchPage(this.swipeUser);
        //Inform other pages to load applied jobs
        this.utilService.newSwipeRight.next(true);
      } else {
        this.swipedJob = removedJob;
        console.log("A disliked Job");
      }
    }

    setTimeout(() => {
      this.animateButton = false;
    }, 700);

  }

  private applyForJob(swipedJobId, currentUserID, jobTitle, userName) {
    this.jobService.applyForJob(swipedJobId, currentUserID, jobTitle, userName).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  //Open Job details Page Modal
  public goToJobDetailsPage(job: Job, prevTab: number) {
    this.modalCtrl.create("JobDetailsPage", { job, prevTab }).present();
  }

  public goToUserDetailsPage(user): void {
    this.modalCtrl.create("UserDetailsPage", { user: user }).present();
  }

  //Refresh Job list
  refresh(): void {
    this.getJobList();
  }

  loadEditModal(){
    let editModal = this.modalCtrl.create('EditProfileInitialPage', { user: this.currentUser });
    editModal.present();
    editModal.onDidDismiss(() => {
      this.storage.get('currentUser')
      .then((user) =>{
        this.currentUser = JSON.parse(user).data;
      })
    });
    return editModal;
  }

  // /**
  //  * --------------------------------------------------------------
  //  *  Open Profile Details Page
  //  * --------------------------------------------------------------
  //  * @method    goToProfileDetailsPage
  //  */
  // goToUserDetailsPage(user) {
  //   this.modalCtrl.create("UserDetailsPage", { user: user }).present();
  // }
  //Open Match Page Modal
  // goToMatchPage(user) {
  //   this.modalCtrl.create("MatchPage", { user: user }).present();
  // }
}

