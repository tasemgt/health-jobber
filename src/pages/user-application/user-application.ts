import { UtilService } from './../../providers/util/util.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { User } from '../../models/user';



@IonicPage()
@Component({
  selector: 'page-user-application',
  templateUrl: 'user-application.html',
})
export class UserApplicationPage {

  user: User;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private alert: AlertController,
    private utilService: UtilService)
  {
    this.user = this.navParams.get('jobseeker');
  }

  public showConfirm(): void{

    let confirm = this.alert.create({
      title: 'Approve Application?',
      message: 'You are about to approve this application. The Applicant will be notified immediately. Are you sure you want to proceed?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {}
        },
        {
          text: 'Proceed',
          handler: () =>{
            console.log('Application approved');
            this.utilService.showToast('Application approved');
            this.navCtrl.pop();
          }
        }
      ]
    });

    confirm.present();
  }

}
