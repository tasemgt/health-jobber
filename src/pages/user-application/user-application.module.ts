import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserApplicationPage } from './user-application';

@NgModule({
  declarations: [
    UserApplicationPage,
  ],
  imports: [
    IonicPageModule.forChild(UserApplicationPage),
  ],
})
export class UserApplicationPageModule {}
