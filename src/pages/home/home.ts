import { Notification } from './../../models/notification';
import { Component } from '@angular/core';
import { IonicPage, NavParams, Platform } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { User } from '../../models/user';
import { UtilService } from '../../providers/util/util.service';
import { NotificationProvider } from '../../providers/notification/notification';
import { LocalNotifications } from '@ionic-native/local-notifications';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage{

  public user: User;
  //public notifications: any[];

  selectedTabIndex: number;

  tab1: string;
  tab2: string;
  tab3: string;
  tab4: string;
  tab5: string;

   constructor(
    private platform: Platform,
    private navParams: NavParams,
    private storage: Storage,
    private util: UtilService,
    private localNotifications: LocalNotifications,
    private notificationProvider: NotificationProvider,
  ) {

    //User details after signin or signup
    this.user = this.navParams.data.data;
    this.selectedTabIndex = this.navParams.get('index');

    this.tab1 = 'ProfilePage';
    this.tab2 = 'SwipePage';
    this.tab3 = 'ApplicationsPage';
    this.tab4 = 'MatchesPage';
    this.tab5 = 'MyJobsPage';
  }

  ionViewDidLoad(){
    this.storage.get('currentUser')
      .then(
        (user) =>{
          if(user){
            user = JSON.parse(user).data;
            setInterval(
              () => {
                this.notificationProvider.getNotifications(user.id)
                  .subscribe(
                    (notifications: Notification[]) => {
                      const unReadNotifications = notifications.filter(notification => !notification.read);
                      console.log(unReadNotifications);

                      if(unReadNotifications.length > 0){
                        unReadNotifications.forEach(
                          (notification) => {

                            const userNotificationId = this.notificationProvider.scheduleLocalNotification('New Job Approval',`Congratulations! ${notification.employer} has approved your application for ${notification.job_title}`, ``, new Date(new Date().getTime() + 1000));

                            this.localNotifications.isScheduled(userNotificationId)
                              .then(
                                () =>{
                                  this.notificationProvider.updateNotification(user.id, notification.id, { read: true })
                                  .subscribe(
                                    () =>{
                                      console.log('update success');
                                    },
                                    () =>{
                                      console.log('update failure');
                                    }
                                  );
                                }
                              );
                          }
                        );
                      }
                    },
                    (error) => {
                      //Error
                    }
                  )
              }, 600000
            );
          }
        }
      );

      if(this.platform.is('cordova')){
        this.localNotifications.on('click')
          .subscribe(
            (notification) => {
              this.selectedTabIndex = 3;
            }
          )
      }
  }


  ionViewWillEnter(){
    if (!this.user || this.util.objectIsEmpty(this.user)) {
      this.getCurrentUser();
    }
  }

  // Fetches the current user from storage.
  private async getCurrentUser(){
    let tempUser: string;
    try {
        tempUser = await this.storage.get("currentUser");
        this.user = JSON.parse(tempUser).data;
    } catch (error) {
      console.log('Cannot get current user');
    }
  }

}

