import { Job } from './../../models/job';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { UserService } from '../../providers/user/user.service';
import { User } from '../../models/user';


@IonicPage()
@Component({
  selector: 'page-my-jobs',
  templateUrl: 'my-jobs.html',
})
export class MyJobsPage {

  currentUser: User;
  myJobs: Job[];


  constructor(private navCtrl: NavController,
              private userService: UserService,
              private modalCtrl: ModalController) {

  }

  //Heavy Lifting and initialization
  ionViewDidLoad(){
    this.userService.getCurrentUserFromStorage().then(
      (user: string) => {
        this.currentUser = JSON.parse(user).data;
        this.userService.getUserById(this.currentUser.id).subscribe(
          (user: User) => {
            this.myJobs = user.jobs;
            console.log(this.myJobs);
          }
        )
      }
    );
  }


  public openAddJobDialog(){
    this.modalCtrl.create('AddJobPage').present();
  }


  public goToMyJobDetailsPage(job: Job) {
    this.navCtrl.push('MyJobDetailsPage', { job });
  }


}
