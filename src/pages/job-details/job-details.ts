import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { JobService } from './../../providers/job/job.service';


import { Job } from '../../models/job';

import { User } from '../../models/user';



@IonicPage()
@Component({
  selector: "page-job-details",
  templateUrl: "job-details.html"
})
export class JobDetailsPage implements OnInit {

  public job: Job;

  currentUser: User;
  prevTab: number;

  constructor(private navCtrl: NavController,  private storage: Storage, private jobService: JobService, private navParams: NavParams) {}

  ngOnInit() {
    this.job = this.navParams.get("job");
    this.prevTab = this.navParams.get('prevTab');
  }

  // Dismisses back to the home page
  dismiss() {
    this.navCtrl.setRoot("HomePage", { index: this.prevTab });
  }

  ionViewDidEnter(){
    if(!this.currentUser){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
           this.currentUser = JSON.parse(user).data;
        });
    }
  }

   public like(status: boolean) {

    // If status is true, current user likes swiped Job user otherwise a dislike
    if (status) {

      if (!this.currentUser) {
        this.storage.get("currentUser").then(user => {
          this.currentUser = JSON.parse(user).data;
          this.applyForJob(this.job.id, this.currentUser.id, this.job.job_title, this.currentUser.name);

          this.dismiss();

        });
      } else {
        this.applyForJob(this.job.id, this.currentUser.id, this.job.job_title, this.currentUser.name);
      }
      //this.goToMatchPage(this.swipeUser);
    }
  }

   private applyForJob(JobId, currentUserID, jobTitle, userName) {
    this.jobService.applyForJob(JobId, currentUserID, jobTitle, userName).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }


}
