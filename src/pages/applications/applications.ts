import { User } from './../../models/user';
import { UserService } from './../../providers/user/user.service';
import { JobService } from './../../providers/job/job.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavParams, ModalController } from 'ionic-angular';
import { Job } from '../../models/job';
import { UtilService } from '../../providers/util/util.service';



@IonicPage()
@Component({
  selector: 'page-applications',
  templateUrl: 'applications.html',
})
export class ApplicationsPage implements OnInit, OnDestroy {

  currentUser: User;
  userAppliedJobs: Job[];
  spin: boolean;

  canReloadData: boolean;
  fromModal: any;

  constructor(private navParams: NavParams, private userService: UserService, private utilService: UtilService, private jobService: JobService, private modalCtrl: ModalController) {
    this.fromModal = this.navParams.get('selectedTabIndex');
  }

  ngOnInit(){
    console.log(this.fromModal);
    if(!this.fromModal){
      this.userService
        .getCurrentUserFromStorage()
        .then((userString: string) => {
          this.currentUser = JSON.parse(userString).data;
          this.getUserApplications(this.currentUser.id);
        })
        .catch(error => console.log(error));

      // Listen for events that a job has been swiped right to
      this.utilService.newSwipeRight.subscribe(
        (reloadData: boolean) => {
          if (reloadData) {
            console.log("reloading data");
            this.canReloadData = true;
          }
        }
      );
    }
  }

  //Get new user appllications on each view enter
  ionViewWillEnter(){
    if(this.canReloadData){
      this.getUserApplications(this.currentUser.id);
      this.canReloadData = false;
    }
  }



  private getUserApplications(userId: number) {
    this.spin = true;
    this.jobService.getUserAppliedJobs(userId).subscribe(
      (jobs: Job[]) => {
        this.spin = false;
        this.userAppliedJobs = jobs;
        console.log(this.userAppliedJobs);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  goToJobDetailsPage(job: Job, prevTab: number){
    this.modalCtrl.create("JobDetailsPage", { job, prevTab }).present();
  }

  ngOnDestroy(){
     //this.utilService.newSwipeRight.unsubscribe();
  }

}
