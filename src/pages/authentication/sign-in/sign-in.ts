import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Component, ViewChild } from '@angular/core';

import { IonicPage, NavController, TextInput } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { UtilService } from "./../../../providers/util/util.service";
import { AuthService } from "./../../../providers/auth/auth.service";



@IonicPage()
@Component({
  selector: "page-sign-in",
  templateUrl: "sign-in.html"
})
export class SignInPage {

  // @ViewChild('signInForm') signInForm: NgForm;
  @ViewChild("email") email: TextInput;
  @ViewChild("password") password: TextInput;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private utilService: UtilService,
    private storage: Storage) {}

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  //SignIn User Handler
  signInUser() {
    var loading = this.utilService.presentLoadingDefault('Signing you in. \nPlease wait ....');
    this.authService
      .signInUser({
        email: this.email.value,
        password: this.password.value
      })
      .subscribe(
        //Success
        (response: HttpResponse<any>) => {
          if (!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              "Login Error",
              "There were problems logging you in, try again please."
            );
          }
          loading.dismiss();
          // this.utilService.showAlert(
          //   "Login Success",
          //   "You have logged in successfully"
          // );
          //Store current loggedIn User for future reference
          this.storage.remove('currentUser');
          this.storage.set("currentUser", JSON.stringify(response.body));
          this.goToProfile(response.body);
        },
        //Error
        (error: HttpErrorResponse) => {
          loading.dismiss();
          console.log(error);
          let message: string;
          if(error.status === 500 || !error.error.errors){
            message = "There were problems logging you in, possible network or server errors, try again please.";
          }
          else{
            if(error.error.errors){
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert( "Login Error", message );
        },
        //Complete error.error.errors[0] ||
        () => {
          console.log("Done!");
        }
      );
  }

  /**
   * --------------------------------------------------------------
   * Go To Profile Page
   * --------------------------------------------------------------
   * @method    goToProfile
   */
  goToProfile(params: object) {
    this.navCtrl.setRoot("HomePage", params);
  }

  /**
   * --------------------------------------------------------------
   * Go To Sign Up Page
   * --------------------------------------------------------------
   * @method    goToSignUpPage
   */
  goToSignUpPage() {
    this.navCtrl.setRoot("SignUpPage");
  }

  /**
   * --------------------------------------------------------------
   * Go To Landing Page
   * --------------------------------------------------------------
   * @method    goToLandingPage
   */
  goToLandingPage() {
    this.navCtrl.setRoot("LandingPage");
  }
}
