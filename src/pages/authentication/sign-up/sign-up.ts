import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";

import { IonicPage, NavController, Slides, TextInput } from 'ionic-angular';

import { UtilService } from './../../../providers/util/util.service';
import { AuthService } from './../../../providers/auth/auth.service';
import { SignUpCredentials } from '../../../models/user';


@IonicPage()
@Component({
  selector: "page-sign-up",
  templateUrl: "sign-up.html"
})
export class SignUpPage {

  public role: string = "jobseeker";

  @ViewChild(Slides) slides: Slides;
  @ViewChild("signUpForm") signUpForm: NgForm;
  @ViewChild("name") name: TextInput;
  @ViewChild("email") email: TextInput;
  @ViewChild("password") password: TextInput;
  @ViewChild("confirmPassword") confirmPassword: TextInput;

  // gender: any = "female";

  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private utilService: UtilService
  ) {}

  ngAfterViewInit() {
    this.slides.lockSwipes(true);
  }

  signUpUser(): void {
    console.log(this.role);
    var user: SignUpCredentials = {
      role: this.role,
      name: this.name.value,
      email: this.email.value,
      password: this.password.value
    };
    var loading = this.utilService.presentLoadingDefault("Signing you up. Please wait ....");
    this.authService.signUpUser(user)
      .subscribe(
        (response: HttpResponse<any>) =>{
          if(!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              'Signup Error',
              'There were problems signing you up, try again please.'
            );
          }
          loading.dismiss();
          this.utilService.showAlert(
            "Signup Success",
            "You have been registered successfully and an email was sent to "+this.email.value+". Confirm your account, and then proceed to login."
          );
          this.navCtrl.setRoot('SignInPage');
          //Store current loggedIn User for future reference
          // this.storage.set("currentUser", JSON.stringify(response.body));
          // this.goToProfile(response.body);
          // if (!response.body.data.edited) {
          //   console.log('Modal created....');
          //   this.modalCtrl.create('EditProfileInitialPage', { user: response.body.data }).present();
          // }
        },
        (error: HttpErrorResponse) =>{
          console.log(error);
          loading.dismiss();
          let message: string;
          if (error.status === 500 || !error.error.errors) {
            message = "There were problems signing you up, possible network or server errors, try again please.";
          }
          else {
            if (error.error.errors) {
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert("Signup Error", message);
        }
      );
  }

  /**
   * --------------------------------------------------------------
   * Go To Profile Page
   * --------------------------------------------------------------
   * @method    goToProfile
   */
  goToProfile(params: object) {
    this.navCtrl.setRoot("HomePage", params);
  }

  /**
   * --------------------------------------------------------------
   * Go To Sign In Page
   * --------------------------------------------------------------
   * @method    goToSignInPage
   */
  goToSignInPage() {
    this.navCtrl.setRoot("SignInPage");
  }

  /**
   * --------------------------------------------------------------
   * Go To Landing Page
   * --------------------------------------------------------------
   * @method    goToLandingPage
   */
  goToLandingPage() {
    this.navCtrl.setRoot("LandingPage");
  }

  /**
   * --------------------------------------------------------------
   * Go To Second Slider
   * --------------------------------------------------------------
   * @method    step2
   */
  step2() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  /**
   * --------------------------------------------------------------
   * Go To Third Slider
   * --------------------------------------------------------------
   * @method    step3
   */
  step3() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }
}
