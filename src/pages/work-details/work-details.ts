
/**
 * @author    Ionic Bucket <ionicbucket@gmail.com>
 * @copyright Copyright (c) 2017
 * @license   Fulcrumy
 *
 * This file represents a component of Edit Profile
 * File path - '../../../../src/pages/edit-profile/edit-profile'
 */
import { IonicPage, NavController, NavParams, TextInput, ViewController} from 'ionic-angular';

import { UtilService } from '../../providers/util/util.service';
import { WorkCredentials } from '../../models/user';

import { UserService } from '../../providers/user/user.service';
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";

import { Storage } from '@ionic/storage';

import md5 from 'crypto-md5';

import { User } from '../../models/user';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";
import { UpdateProvider } from '../../providers/update/update';


@IonicPage()
@Component({
  selector: 'page-work-details',
  templateUrl: 'work-details.html',
})

export class WorkDetailsPage {

  profilePicture: any = "https://www.gravatar.com/avatar/"
  currentUser: User;

   public userid: any;
   public work_details: any;

  @ViewChild("updateWorkForm") updateWorkForm: NgForm;
  @ViewChild("job_title") job_title: TextInput;
  @ViewChild("company") company: TextInput;
  @ViewChild("responsibilities") responsibilities: TextInput;
  @ViewChild("start_date") start_date: TextInput;
  @ViewChild("end_date") end_date: TextInput;




  public base64Image: string;
  img1: any = 'assets/imgs/13.jpg';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
      private storage: Storage,
     private utilService: UtilService,
     private userService: UserService,
    public viewCtrl: ViewController,
    public updateService: UpdateProvider) {

     //User details after signin or signup
  }

  //Every snap at the profie tab view
  ionViewWillEnter(){
    if(!this.currentUser){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
           this.currentUser = JSON.parse(user).data;
           var email = (JSON.parse(user).data.email);
           this.profilePicture = "https://www.gravatar.com/avatar/"+md5(email.toLowerCase()+"?d='wavatar'", 'hex');
           this.userid = this.currentUser.id;
           console.log(this.currentUser.id);
           this.getWorkDetails(this.currentUser.id);
        });
    }

  }

  // ionViewWillEnter() {
  //   this.getWorkDetails(this.userid);
  // }




   updateWork(): void {
    // console.log(this.work);

      if(!this.currentUser){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
           this.currentUser = JSON.parse(user).data;
           this.userid = (JSON.parse(user).data.id);

        });
    }

    var workDetails: WorkCredentials = {
      user_id: this.userid,
      job_title: this.job_title.value,
      company: this.company.value,
      responsibilities: this.responsibilities.value,
      start_date: this.start_date.value,
      end_date: this.end_date.value
    };

    console.log(workDetails);
    var loading = this.utilService.presentLoadingDefault("Updating Work Details ...");
    loading.dismiss();
    this.userService.updateWork(workDetails)
      .subscribe(
        (response: HttpResponse<any>) =>{
          if(!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              'Error Updting Work Details',
              'There were problems updating your work details, try again please.'
            );
          }
          loading.dismiss();
          this.utilService.showAlert(
            "Update Successful",
            "Your details were saved successfully"
          );
          this.updateWorkForm.reset();
          this.getWorkDetails(this.currentUser.id);
          // this.openEditProfilePage();
        },
        (error: HttpErrorResponse) =>{
          console.log(error);
          loading.dismiss();
          let message: string;
          if (error.status === 500 || !error.error.errors) {
            message = "There were problems updating your work details. possible network or server errors, try again please.";
          }
          else {
            if (error.error.errors) {
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert("Update Error", message);
        }
      );
  }

  // getWorkDetails(userId) {

  //   // this.initialJobSearch = true;
  //   var loading = this.utilService.presentLoadingDefault("Fetching Work History ...");

  //   this.updateService.fetchWorkDetails(userId).subscribe(
  //     (response: WorkCredentials[]) => {
  //       this.work_details = response;
  //       console.log(this.work_details);
  //        loading.dismiss();
  //     },
  //     (error: HttpErrorResponse) => {
  //       console.error(error.message);
  //        loading.dismiss();
  //       this.utilService.showToast(
  //         "Cannot fetch Work History, possible connection problems..."
  //       );

  //     }
  //   );



  // }


  getWorkDetails(userId) {
     var loading = this.utilService.presentLoadingDefault("Fetching Work History ...");

     this.updateService.fetchWorkDetails(userId).then( (result) => {
     this.work_details = result;
     console.log(this.work_details);
     loading.dismiss();
      // this.presentToast('Post Created Successfully');
      // this.navCtrl.setRoot(CommunityPage);
    },  (error: HttpErrorResponse) => {
        console.error(error.message);
         loading.dismiss();
        this.utilService.showToast(
          "Cannot fetch Work History, possible connection problems..."
        );
        // setTimeout(() => {
        //   this.getJobList();
        // }, 10000);
      }
    );


  }



  //  updateProfile(): void {
  //   // console.log(this.work);

  //     if(!this.currentUser){
  //     this.storage.get('currentUser')
  //       .then((user) =>{
  //         console.log(user);
  //          this.currentUser = JSON.parse(user).data;
  //          this.userid = (JSON.parse(user).data.id);

  //       });
  //   }

  //   var profileDetails: ProfileDetails = {
  //     user_id: this.userid,
  //     name: this.name.value,
  //     about: this.about.value,
  //     gender: this.gender.value,
  //     phone: this.phone.value,
  //     address: this.address.value,
  //     dob: this.dob.value
  //   };

  //   console.log(profileDetails);
  //   var loading = this.utilService.presentLoadingDefault("Updating Profile Details ...");
  //   loading.dismiss();
  //   this.userService.updateProfile(profileDetails)
  //     .subscribe(
  //       (response: HttpResponse<any>) =>{
  //         if(!response.ok) {
  //           loading.dismiss();
  //           return this.utilService.showAlert(
  //             'Error Updting Profile Details',
  //             'There were problems updating your profile details, try again please.'
  //           );
  //         }
  //         loading.dismiss();
  //         this.utilService.showAlert(
  //           "Update Successful",
  //           "Your details were saved successfully"
  //         );
  //         // this.updateProfileForm.reset();
  //         // this.openEditProfilePage();
  //       },
  //       (error: HttpErrorResponse) =>{
  //         console.log(error);
  //         loading.dismiss();
  //         let message: string;
  //         if (error.status === 500 || !error.error.errors) {
  //           message = "There were problems updating your profile details. possible network or server errors, try again please.";
  //         }
  //         else {
  //           if (error.error.errors) {
  //             message = error.error.errors[0]
  //           }
  //         }
  //         this.utilService.showAlert("Update Error", message);
  //       }
  //     );
  // }



  /**
   * This function dismiss the popup modal
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
