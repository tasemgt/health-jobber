import { SocialSharing } from '@ionic-native/social-sharing/';
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Component } from '@angular/core';

import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { UtilService } from "../../providers/util/util.service";
import { SmartCvProvider } from "../../providers/smart-cv/smart-cv";

import md5 from 'crypto-md5';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  women: Boolean = true;
  man: Boolean = false;
  distance: any = 23
  ages: any;
  visibility: Boolean = true;
  showSmartCv: Boolean = false;
  userId: any;
  profilePicture: any;

  constructor(
    private navCtrl: NavController,
    private viewCtrl: ViewController,
    private utilService: UtilService,
    private socialSharing: SocialSharing,
    public smartCvService: SmartCvProvider,
    private storage: Storage) {

    if(!this.userId){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
           this.userId = (JSON.parse(user).data.id);
           var email = (JSON.parse(user).data.email);
           this.profilePicture = "https://www.gravatar.com/avatar/"+md5(email.toLowerCase()+"?d=wavatar", 'hex');

        });
    }
  }
  share(){
    this.socialSharing.share("An Aggregator for on demand Healthcare Jobs in Nigeria. Register Now", "HealthJobber", "https://i.ibb.co/hXFNf1g/hjad.png", "https://healthjobber.herokuapp.com").then(() => {
    }).catch(() => {
    });
  }

  ngAfterViewInit(): void {
    this.ages = { lower: 37, upper: 68 };
  }

  toggleSmartCv() {
    this.showSmartCv = !this.showSmartCv;
  }

  generateSmartCv() {
    console.log("generating smart cv");
     var loading = this.utilService.presentLoadingDefault('Generating your smart CV. \nPlease wait ....');

       //beginning of smart cv service
        this.smartCvService
      .generateCv(this.userId)
      .subscribe(
        //Success
        (response: HttpResponse<any>) => {
          if (!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              "CV Error",
              "There were problems generating your smart cv. please try again or contact support."
            );
          }

          this.storage.set("smartCv", JSON.stringify(response.body));
          console.log(JSON.stringify(response.body));

          loading.dismiss();

          //load modal with cv body


        },
        // Error
          (error: HttpErrorResponse) => {
          loading.dismiss();
          console.log(error);
          let message: string;
          if(error.status === 500 || !error.error.errors){
            message = "There were problems generating your smart cv, possible network or server timeout, please try again.";
          }
          else{
            if(error.error.errors){
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert( "An Error Occurred", message );
        },
        //Complete error.error.errors[0] ||
        () => {
          console.log("Done!");
        }
      );

  }

  comingSoon(){
    this.utilService.showAlert( "Coming Soon", "Feature will be shipped in Version 2 of HealthJobber" );
  }

  goToUrl(url){
    window.open(url); return false;
  }

  // Logout
  logout() {
    this.storage.remove('currentUser')
      .then(() =>{
        this.navCtrl.setRoot('LandingPage');
      })
      .catch(error => console.log(error));
  }
  /**
   * This function dismiss the popup modal
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
