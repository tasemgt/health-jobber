import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Job } from '../../models/job';
import { User } from '../../models/user';
import { UserService } from '../../providers/user/user.service';



@IonicPage()
@Component({
  selector: "page-my-job-details",
  templateUrl: "my-job-details.html"
})
export class MyJobDetailsPage {
  job: Job;
  jobseekers: User[];
  spin: boolean;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private userService: UserService
  ) {
    this.job = this.navParams.get("job");
  }

  ionViewDidLoad(){
    this.spin = true;
    this.userService
      .getJobApplicationUsers(this.job.id)
      .subscribe((users: User[]) => {
        this.spin = false;
        this.jobseekers = users;
      });
  }

  /**
   * goToUserApplicationPage
   */
  public goToUserApplicationPage(jobseeker: User) {
    this.navCtrl.push("UserApplicationPage", { jobseeker });
  }
}
