import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyJobDetailsPage } from './my-job-details';

@NgModule({
  declarations: [
    MyJobDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyJobDetailsPage),
  ],
})
export class MyJobDetailsPageModule {}
