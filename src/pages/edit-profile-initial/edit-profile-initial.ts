import { IonicPage, NavController, NavParams, ModalController, TextInput, ViewController} from 'ionic-angular';
import { UtilService } from '../../providers/util/util.service';

import { UserService } from '../../providers/user/user.service';
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";

import { Storage } from '@ionic/storage';

import { User } from '../../models/user';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";


@IonicPage()
@Component({
  selector: 'page-edit-profile-initial',
  templateUrl: 'edit-profile-initial.html',
})

export class EditProfileInitialPage {

  profilePicture: string;
  profileInitials: string;

  currentUser: User;

  public userid: any;

  @ViewChild("updateProfileForm") updateProfileForm: NgForm;
  @ViewChild("user_about") about: TextInput;
  @ViewChild("user_phone") phone: TextInput;
  @ViewChild("user_dob") dob: TextInput;
  @ViewChild("user_gender") gender: TextInput;
  @ViewChild("user_address") address: TextInput;
  @ViewChild("user_country") country: TextInput;
  @ViewChild("user_state") state: TextInput;



  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private modalCtrl: ModalController,
    private utilService: UtilService,
    private userService: UserService,
    public viewCtrl: ViewController
    ) {
  }


  ionViewWillLoad(){
    this.storage.get('currentUser')
      .then((user) => {
        console.log(user);
        this.currentUser = JSON.parse(user).data;
        this.dob.value = (new Date(this.currentUser.dob)).toISOString();
        console.log(this.dob.value);
        this.gender.value = this.currentUser.gender ? this.currentUser.gender.toLowerCase() : "";
      });
  }

   updateProfile(): void {

    var profileDetails = {
      user_id: this.currentUser.id,
      about: this.about.value,
      gender: this.gender.value,
      phone: this.phone.value,
      address: this.address.value,
      dob: this.utilService.refineDate(this.dob.value), //Refines date to format yyyy-mm-dd
      country: this.country.value,
      state: this.state.value
    };

    console.log(this.dob.value);
    console.log(profileDetails);
    var loading = this.utilService.presentLoadingDefault("Updating Profile Details ...");
    this.userService.updateProfile(profileDetails)
      .subscribe(
        (response: HttpResponse<any>) =>{
          if(!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              'Error Updting Profile Details',
              'There were problems updating your profile details, try again please.'
            );
          }
          this.userService.getUserById(this.currentUser.id).toPromise()
            .then((user: User) => {
              this.storage.set('currentUser', JSON.stringify({ data: user })); //using {data:user} to match other saves to the local storage
              loading.dismiss();
              this.utilService.showToast('Update Successful...');
            });
          this.viewCtrl.dismiss();
        },
        (error: HttpErrorResponse) =>{
          console.log(error);
          loading.dismiss();
          let message: string;
          if (error.status === 500 || !error.error.errors) {
            message = "There were problems updating your profile details. possible network or server errors, try again please.";
          }
          else {
            if (error.error.errors) {
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert("Update Error", message);
        }
      );
  }


    /**
   * --------------------------------------------------------------
   * Open Edit Profile Page
   * --------------------------------------------------------------
   * @method    openEditProfilePage
   */
  openEditProfilePage() {
    this.modalCtrl.create('EditProfilePage').present();
  }

  openWorkDetailsPage() {
    this.modalCtrl.create('WorkDetailsPage').present();

  }

  dismiss() {
    this.utilService.presentConfirm('Cancel Editing?', 'If you do not edit your profile, you can view but won\'t be able to apply for jobs. \n Continue without Editing?', [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          console.log('Yes Clicked...');
          this.viewCtrl.dismiss();
        }
      }
    ]);
  }
}
