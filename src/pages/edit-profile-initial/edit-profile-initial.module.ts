import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditProfileInitialPage } from './edit-profile-initial';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    EditProfileInitialPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(EditProfileInitialPage),
  ],
})
export class EditProfilePageInitialModule { }
