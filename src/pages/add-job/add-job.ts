import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-add-job',
  templateUrl: 'add-job.html',
})
export class AddJobPage {

  constructor(
    private viewCtrl: ViewController) {
  }


  public dismiss(): void{
    this.viewCtrl.dismiss();
  }

}
