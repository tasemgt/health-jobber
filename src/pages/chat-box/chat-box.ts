import { UserService } from './../../providers/user/user.service';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Content, Slides } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Chat } from '../../models/chat';
import { ChatProvider } from '../../providers/chat/chat';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user';

import * as firebase from 'firebase';
import { FirebaseService } from '../../providers/util/firebase.service';
import { NotificationProvider } from '../../providers/notification/notification';

@IonicPage()
@Component({
  selector: "page-chat-box",
  templateUrl: "chat-box.html"
})
export class ChatBoxPage {
  @ViewChild("content")
  content: Content;
  @ViewChild("mySlider")
  slider: Slides;

  // Chat Form that handles sending of messages
  chatForm: FormGroup;
  // Array List of Chat History
  chats: Chat[] = [];
  // List of chat reply message
  chatReplyMessages: any = this.chatProvider.randomChatReplyMsg();
  // List of GIF
  gifs: any = [];
  // GIF Images Limit
  limit: number = 25;
  // Search GIF Image
  searchText: string;
  // Show/Hide GIF Slider Contents
  showGif: Boolean = false;

  currentUser: User;
  receiverId: number;
  isEmployer: boolean = false;
  receiver: User;
  chatId: string;
  chatNames: string;
  jobId: string;
  jobTitle: string;
  employer: User;
  jobseeker: User;
  canNotify: boolean = true;

  isCleanChat: boolean = false;

  chatbaseRef: any = firebase.database().ref("chatbase/");

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private chatProvider: ChatProvider,
    private formBuilder: FormBuilder,
    private storage: Storage,
    private userService: UserService,
    private notificationProvider: NotificationProvider,
    private firebaseService: FirebaseService
  ) {}

  /** Do any initialization */
  ionViewWillLoad() {
    console.log("inside chat");
    this.setupChatForm();

    this.receiverId = this.navParams.get("receiverId") || "3";
    this.jobId = this.navParams.get("jobId") || 29;
    this.jobTitle = this.navParams.get("jobTitle") || "Unknown Job";

    this.userService.getUserById(this.receiverId).subscribe((user: User) => {
      this.receiver = user;
      console.log(this.receiver);
      this.storage.get("currentUser").then(user => {
        this.currentUser = JSON.parse(user).data;
        console.log(this.currentUser);
        if (this.currentUser.role === "employer") {
          this.isEmployer = true;
          this.chatId = `${this.currentUser.id}_${this.receiverId}`;
          this.employer = this.currentUser;
          this.jobseeker = this.receiver;
        } else {
          this.chatId = `${this.receiverId}_${this.currentUser.id}`;
          this.employer = this.receiver;
          this.jobseeker = this.currentUser;
        }
        this.canNotify = false;
        this.getChatHistory();
      });
    });
  }

  setupChatForm() {
    // Setup form
    this.chatForm = this.formBuilder.group({
      message: ["", Validators.required]
    });
  }

  getChatHistory() {
    if (this.chatId && this.jobId) {
      firebase
        .database()
        .ref(`chatbase/${this.chatId}/${this.jobId}/chats`)
        .on("value", resp => {
          //Convert Datasnapshot to chats array
          this.chats = this.firebaseService.snapshotToArray(resp);
          //console.log(this.chats, this.chatId.toString().substring(0, this.chatId.toString().indexOf('_')));

          let chat: Chat = this.chats[this.chats.length - 1];

          if(this.canNotify && !(chat.senderId === this.currentUser.id)){
            this.notificationProvider.scheduleLocalNotification(`New message from ${chat.senderName}`, `${chat.message}`, ``, new Date(new Date().getTime() + 50));
            console.log("From chat-box");
          }

          if (this.chats.length < 1) {
            this.isCleanChat = true;
          }
          this.scrollToBottom();
        });
    }
    this.canNotify = true;
  }

  onMessageInputFocus() {
    setTimeout(() => this.scrollToBottom(), 100);
  }

  sendMsg() {
    let message = this.chatForm.controls["message"].value;

    if (message && !this.isAllSpaces(message)) {
      let chatRef = firebase
        .database()
        .ref(`chatbase/${this.chatId}/${this.jobId}/chats`)
        .push();

      let chat: Chat = {
        senderId: this.currentUser.id,
        receiverId: this.receiverId,
        senderName: this.currentUser.name,
        receiverName: this.receiver.name,
        message: message,
        timestamp: new Date().toString()
      };

      chatRef.set(chat);
      console.log(chat);
      if (this.isCleanChat) {
        let currentChatRef = firebase
          .database()
          .ref(`currentchatbase/${this.chatId}`)
          .push();

        let currentChat = {
          employer: this.employer,
          jobseeker: this.jobseeker,
          job_title: this.jobTitle,
          job_id: this.jobId
        };
        currentChatRef.set(currentChat);
        this.isCleanChat = false;
      }

      // Clear input field
      this.chatForm.controls["message"].setValue("");

      this.scrollToBottom();
    }
    // Clear input field
    this.chatForm.controls["message"].setValue("");
  }

  //This function dismiss the popup modal
  dismiss() {
    this.viewCtrl.dismiss();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content && this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 1000);
  }

  private isAllSpaces(str: string): boolean {
    if (!str.replace(/\s/g, "").length) {
      return true;
    }
    return false;
  }

  // replyMessage() {
  //   let replyMes = this.chatReplyMessages[
  //     Math.floor(Math.random() * this.chatReplyMessages.length)
  //   ];

  //   //Simulate response after delay
  //   setTimeout(() => {
  //     this.chats.push({
  //       senderId: 4,
  //       receiverId: 4,
  //       senderName: "Alidia Parks",
  //       receiverName: "Tom Cruise",
  //       message: replyMes,
  //       status: "received",
  //       timestamp: new Date()
  //     });
  //     this.scrollToBottom();
  //   }, 1000);
  // }

  // sendGif() {

  //   // Selected Gif Image Index
  //   let theClickedIndex = this.slider.clickedIndex;

  //   // Selected Gif Image
  //   let msgGifImg = this.gifs[theClickedIndex].images.preview_gif.url;

  //   if (msgGifImg) {
  //     this.chats.push({
  //       senderName: "Tom Cruise",
  //       receiverName: "Alidia Parks",
  //       image: msgGifImg,
  //       status: "sent",
  //       timestamp: new Date()
  //     });
  //   }

  //   setTimeout(() => this.scrollToBottom(), 100);

  //   this.replyMessage();
  // }

  //getGifs() {
  //   this.giphyProvider.getTrendingGifs(this.limit).subscribe((data: any) => {
  //     this.gifs.push(...data.data);
  //     this.limit = this.limit + 5;
  //     this.scrollToBottom();
  //   });
  // }

  // searchGifs() {
  //   this.giphyProvider.searchGif(this.searchText).subscribe((data: any) => {
  //     this.gifs = data.data;
  //     this.scrollToBottom();
  //   });
  // }
}
