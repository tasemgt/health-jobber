import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatBoxPage } from './chat-box';

import { MomentModule } from "ngx-moment";

@NgModule({
  declarations: [
    ChatBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatBoxPage),
    MomentModule
  ],
})
export class ChatBoxPageModule {}
