import { IonicPage, NavController, NavParams, ModalController, TextInput, ViewController} from 'ionic-angular';
import { UtilService } from '../../providers/util/util.service';
import { WorkCredentials } from '../../models/user';
import { ProfileDetails } from '../../models/user';

import { UserService } from '../../providers/user/user.service';
import { HttpResponse, HttpErrorResponse } from "@angular/common/http";

import { Storage } from '@ionic/storage';

// import md5 from 'crypto-md5';

import { User } from '../../models/user';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms";


@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})

export class EditProfilePage {

  profilePicture: string;
  profileInitials: string;

  currentUser: User;

  public userid: any;

  @ViewChild("updateWorkForm") updateWorkForm: NgForm;
  @ViewChild("job_title") job_title: TextInput;
  @ViewChild("company") company: TextInput;
  @ViewChild("responsibilities") responsibilities: TextInput;
  @ViewChild("start_date") start_date: TextInput;
  @ViewChild("end_date") end_date: TextInput;

  @ViewChild("updateProfileForm") updateProfileForm: NgForm;
  @ViewChild("user_name") name: TextInput;
  @ViewChild("user_about") about: TextInput;
  @ViewChild("user_phone") phone: TextInput;
  @ViewChild("user_dob") dob: TextInput;
  @ViewChild("user_gender") gender: TextInput;
  @ViewChild("user_address") address: TextInput;
  @ViewChild("user_state") state: TextInput;
  @ViewChild("user_country") country: TextInput;


  public base64Image: string;
  img1: any = 'assets/imgs/13.jpg';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private modalCtrl: ModalController,
    private utilService: UtilService,
    private userService: UserService,
    public viewCtrl: ViewController) {

  }

  //Every snap at the profie tab view
  ionViewDidLoad(){
    if(!this.currentUser){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
          this.currentUser = JSON.parse(user).data;
          let name: string = JSON.parse(user).data.name;
          // var email: string = (JSON.parse(user).data.email);
          this.userid = (JSON.parse(user).data.id);

          this.dob.value = (new Date(this.currentUser.dob)).toISOString();
          this.gender.value = this.currentUser.gender? this.currentUser.gender.toLowerCase() : "";

          this.profileInitials = name.charAt(0).concat(name.charAt(name.indexOf(' ') + 1));
          this.profilePicture = "";//"https://www.gravatar.com/avatar/"+md5(email.toLowerCase()+"?d='wavatar'", 'hex');

        });
    }
  }

  comingSoon(){
    this.utilService.showAlert( "Coming Soon", "Feature will be shipped in Version 2 of HealthJobber" );
  }




   updateWork(): void {
    // console.log(this.work);

      if(!this.currentUser){
      this.storage.get('currentUser')
        .then((user) =>{
          console.log(user);
           this.currentUser = JSON.parse(user).data;
           this.userid = (JSON.parse(user).data.id);

        });
    }

    var workDetails: WorkCredentials = {
      user_id: this.userid,
      job_title: this.job_title.value,
      company: this.company.value,
      responsibilities: this.responsibilities.value,
      start_date: this.start_date.value,
      end_date: this.end_date.value
    };

    console.log(workDetails);
    var loading = this.utilService.presentLoadingDefault("Updating Work Details ...");
    loading.dismiss();
    this.userService.updateWork(workDetails)
      .subscribe(
        (response: HttpResponse<any>) =>{
          if(!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              'Error Updting Work Details',
              'There were problems updating your work details, try again please.'
            );
          }
          loading.dismiss();
          this.utilService.showAlert(
            "Update Successful",
            "Your details were saved successfully"
          );
          this.updateWorkForm.reset();
          // this.openEditProfilePage();
        },
        (error: HttpErrorResponse) =>{
          console.log(error);
          loading.dismiss();
          let message: string;
          if (error.status === 500 || !error.error.errors) {
            message = "There were problems updating your work details. possible network or server errors, try again please.";
          }
          else {
            if (error.error.errors) {
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert("Update Error", message);
        }
      );
  }



   updateProfile(): void {
    // console.log(this.work);

    //   if(!this.currentUser){
    //   this.storage.get('currentUser')
    //     .then((user) =>{
    //       console.log(user);
    //        this.currentUser = JSON.parse(user).data;
    //        this.userid = (JSON.parse(user).data.id);

    //     });
    // }

    var profileDetails: ProfileDetails = {
      user_id: this.userid,
      name: this.name.value || "",
      about: this.about.value || "",
      gender: this.gender.value || "",
      phone: this.phone.value || "",
      address: this.address.value || "",
      country: this.country.value || "",
      state: this.state.value || "",
      dob: this.utilService.refineDate(this.dob.value) || "" //Refines date to format yyyy-mm-dd
    };

    var loading = this.utilService.presentLoadingDefault("Updating Profile Details ...");
    this.userService.updateProfile(profileDetails)
      .subscribe(
        (response: HttpResponse<any>) =>{
          if(!response.ok) {
            loading.dismiss();
            return this.utilService.showAlert(
              'Error Updting Profile Details',
              'There were problems updating your profile details, try again please.'
            );
          }
          this.userService.getUserById(this.userid).toPromise()
            .then((user: User) =>{
              this.storage.set('currentUser', JSON.stringify({data:user})); //using {data:user} to match other saves to the local storage
              loading.dismiss();
              console.log({data:user})
              this.utilService.showAlert(
                "Update Successful",
                "Your details were saved successfully"
              );
            });
        },
        (error: HttpErrorResponse) =>{
          console.log(error);
          loading.dismiss();
          let message: string;
          if (error.status === 500 || !error.error.errors) {
            message = "There were problems updating your profile details. possible network or server errors, try again please.";
          }
          else {
            if (error.error.errors) {
              message = error.error.errors[0]
            }
          }
          this.utilService.showAlert("Update Error", message);
        }
      );
  }




    /**
   * --------------------------------------------------------------
   * Open Edit Profile Page
   * --------------------------------------------------------------
   * @method    openEditProfilePage
   */
  openEditProfilePage() {
    this.modalCtrl.create('EditProfilePage').present();
  }

  openWorkDetailsPage() {
    this.modalCtrl.create('WorkDetailsPage').present();

  }



  /**
   * --------------------------------------------------------------
   * Open Gallery & Select Photo
   * --------------------------------------------------------------
   * @method    takePhoto
   * @param     num    Image Number
   */
  // takePhoto(num) {
  //   // Camera Options
  //   const options: CameraOptions = {
  //     quality: 50,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //     allowEdit: true,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     targetWidth: 500,
  //     targetHeight: 500
  //   }
  //   this.camera.getPicture(options).then((base64String: string) => {
  //     this.base64Image = "data:image/*;base64," + base64String;

  //     if (num == 1) {
  //       this.img1 = this.base64Image;
  //     }
  //     if (num == 2) {
  //       this.img2 = this.base64Image;
  //     }
  //     if (num == 3) {
  //       this.img3 = this.base64Image;
  //     }
  //     if (num == 4) {
  //       this.img4 = this.base64Image;
  //     }
  //     if (num == 5) {
  //       this.img5 = this.base64Image;
  //     }
  //     if (num == 6) {
  //       this.img6 = this.base64Image;
  //     }
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  /**
   * This function dismiss the popup modal
   */
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
