import { UtilService } from './../../providers/util/util.service';
import { JobService } from './../../providers/job/job.service';
import { Application } from './../../models/application';
import { User } from './../../models/user';
import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, ModalController, Slides } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { UserService } from '../../providers/user/user.service';
import { Job } from '../../models/job';

import * as firebase from "firebase";
import { FirebaseService } from '../../providers/util/firebase.service';


@IonicPage()
@Component({
  selector: 'page-matches',
  templateUrl: 'matches.html',
})
export class MatchesPage implements OnInit, OnDestroy {

  @ViewChild('jobslider') jobslider: Slides;

  users: any;
  currentChats: any = [];
  currentUser: User;
  employers: User[];
  appliedJobs: Job[];
  approvedApplications: Application[];
  matchedJobs: Job[];
  spin1: boolean = true;
  spin2: boolean = true;
  jobTitle: string = "";
  canReloadData: boolean;
  listForEmployer: {jobseeker: User, jobTitle: string, jobId: number}[];
  listForJobseeker: {employer: User, jobTitle: string, jobId: number}[];


  constructor(public navCtrl: NavController,
    private dataProvider: DataProvider,
    private modalCtrl: ModalController,
    private userService: UserService,
    private jobService: JobService,
    private utilService: UtilService,
    private firebaseService: FirebaseService
  ) {}

  //Fetch current user from storage, get employers list
  ngOnInit() {
    this.userService
      .getCurrentUserFromStorage()
      .then((userString: string) => {
        this.currentUser = JSON.parse(userString).data;
        this.currentUser.role === 'jobseeker' ?
          this.getUserMatchedJobs(this.currentUser.id) : this.getApprovedApplicationsByEmployer(this.currentUser.id);
      })
      .catch(error => console.log(error));

    // Listen for events that a job has been swiped right to
    this.utilService.newSwipeRight.subscribe(
      (reloadData: boolean) => {
        if (reloadData) {
          console.log("reloading data");
          this.canReloadData = true;
        }
      }
    );
  }


  //Get User applied jobs on each view enter
  ionViewWillEnter(){
    this.getUserChats();
    if(this.canReloadData){
      this.matchedJobs = [];
      //Load approved applications if a jobseeker, otherwise load approved users.
      this.currentUser.role === 'jobseeker' ?
      this.getUserMatchedJobs(this.currentUser.id) : this.getApprovedApplicationsByEmployer(this.currentUser.id)
      this.canReloadData = false;
    }
  }

  getUserList() {
    this.dataProvider.getUserList().subscribe((data: any) => {
      this.users = data;
    });
  }


  //Fetches jobs approved by an employer for a user.
  private getUserMatchedJobs(userId: number){
    this.spin1 = true;
    this.jobService.getUserMatchedJobs(userId).subscribe(
      (jobs: Job[]) =>{
        this.matchedJobs = jobs;
        this.spin1 = false;
        console.log(this.matchedJobs);
      },
      (error) => console.log(error)
    )
  }

  //Fetches all applications approved by an employer
  private getApprovedApplicationsByEmployer(empId: number){
    this.spin1 =true;
    this.jobService.getApprovedApplicationsByEmployer(empId).subscribe(
      (applications: Application[]) =>{
        this.approvedApplications = applications;
        this.spin1 = false;
        console.log(this.approvedApplications);
      },
      (error) => console.log(error)
    )
  }


  //Fetches and syncs all recent chats by a user.
  private getUserChats(){
    firebase
      .database()
      .ref(`currentchatbase`)
      .on('value', resp =>{

        this.currentChats = [];
        let refinedSnapshotArray = [];
        let pattern;

        this.listForEmployer = [];
        this.listForJobseeker = [];

        refinedSnapshotArray = this.firebaseService.snapshotToArray(resp);

        //Create match patterns based off on user role
        //Employer_jobseeker
        if(this.currentUser.role === 'jobseeker'){
          pattern = new RegExp(`_${this.currentUser.id}`, "g");
          refinedSnapshotArray.forEach(chatKey => {
            if (chatKey.key.match(pattern)) {
              this.currentChats.push(chatKey);
            }
          });
        }
        else{
          pattern = new RegExp(`${this.currentUser.id}_`, "g");
          refinedSnapshotArray.forEach(chatKey => {
            if (chatKey.key.match(pattern)) {
              this.currentChats.push(chatKey);
            }
          });
        }

        this.currentChats.forEach(currentChat => {
          let arr = Object.keys(currentChat).filter(key => key !== 'key');
          arr.forEach((combination) =>{ //Combination is the collection of jobseeker, employer, job_title, and job_id in an object
            if (this.currentUser.role === 'employer') {
              let displayJobseeker = { jobseeker: currentChat[combination].jobseeker, jobTitle: currentChat[combination].job_title, jobId: currentChat[combination].job_id };
              this.listForEmployer.push(displayJobseeker);
            }
            else {
              let displayEmployer = { employer: currentChat[combination].employer, jobTitle: currentChat[combination].job_title, jobId: currentChat[combination].job_id };
              this.listForJobseeker.push(displayEmployer);
            }
          });
        });
        this.spin2 = false;
      });
  }

  //Open Chat Page with receiverId and jobId
  public openChatBox(receiverId: number, jobId: number, jobTitle: string) {
    this.modalCtrl.create('ChatBoxPage', { receiverId, jobId, jobTitle }).present();
  }

  ngOnDestroy(){
    // this.utilService.newSwipeRight.unsubscribe();
  }


}
