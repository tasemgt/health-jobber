import { Component } from '@angular/core';
import { IonicPage, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

// import md5 from 'crypto-md5';

import { UtilService } from "../../providers/util/util.service";
import { DataProvider } from '../../providers/data/data';
import { User } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profilePicture: string;

  profileInitials: string;

  currentUser: User;
  sliderData: any;
  email: any;

  constructor(
    private navParams: NavParams,
    private storage: Storage,
    private dataProvider: DataProvider,
    private utilService: UtilService,
    private modalCtrl: ModalController) {

    //User details after signin or signup
    this.currentUser = this.navParams.data.data;
    console.log(this.currentUser);
  }

  comingSoon() {
    this.utilService.showAlert( "Coming Soon", "Feature will be shipped in Version 2 of HealthJobber" );
  }

  /** Do any initialization */
  ngOnInit() {
    this.getSliderData();
  }


  //Every snap at the profie tab view
  ionViewWillEnter(){
    if (!this.currentUser) {
      this.storage.get('currentUser')
        .then((user) => {
          console.log(user);
          this.currentUser = JSON.parse(user).data;
          // const email: string = (JSON.parse(user).data.email);
          let name: string = JSON.parse(user).data.name;
          this.profileInitials = name.charAt(0).concat(name.charAt(name.indexOf(' ')+1));
          this.profilePicture = "";//"https://www.gravatar.com/avatar/"+md5(email.toLowerCase()+"?d=wavatar", 'hex');
        });
    }
  }


  getSliderData() {
    this.sliderData = this.dataProvider.profileSlider();
  }

  openEditProfilePage() {
    this.modalCtrl.create('EditProfilePage').present();
  }

  openSettingsPage() {
    this.modalCtrl.create('SettingsPage').present();
  }
}


