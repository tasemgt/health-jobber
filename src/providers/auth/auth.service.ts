import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SignUpCredentials } from '../../models/user';
import { Credentials } from "../../models/user";

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1";

  //Registers user
  signUpUser(user: SignUpCredentials) {
    return this.http.post(`${this.baseUrl}/auth`, user, { observe: 'response' });
  }

  //Signin user
  signInUser(credentials: Credentials) {
    return this.http.post(`${this.baseUrl}/auth/sign_in`, credentials, { observe: 'response'});
  }
}
