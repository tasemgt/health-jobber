import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class JobService {

  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1";

  constructor(private http: HttpClient) {}

  //Fetches All Jobs from server..
  getJobList(){
    return this.http.get(`${this.baseUrl}/jobs`);
  }

  //Gets all jobs applied by a user
  getUserAppliedJobs(userId: number){
    return this.http.get(`${this.baseUrl}/users/${userId}/jobs_applied_for`);
  }

  //Gets all applications applied by a user
  getUserApplications(userId: number){
    return this.http.get(`${this.baseUrl}/users/${userId}/applications`);
  }

  //Gets all applications applied under a job
  getJobApplications(userId: number, jobId: number){
    return this.http.get(`${this.baseUrl}/users/${userId}/jobs/${jobId}/applications`);
  }

  //Get all approved applications by employer
  getApprovedApplicationsByEmployer(empId: number){
    return this.http.get(`${this.baseUrl}/users/${empId}/applications_approved`);
  }

  //Gets all jobs approved for a user
  getUserMatchedJobs(userId: number){
    return this.http.get(`${this.baseUrl}/users/${userId}/jobs_approved`)
  }

  //Matches user with currently interested job
  applyForJob(job_id: number, user_id: number, job_title: string, user_name: string){
    let application = {
      application: {user_id, job_title, user_name}
    }
    console.log(application);
    return this.http.post(`${this.baseUrl}/jobs/${job_id}/applications`, application);
  }
}
