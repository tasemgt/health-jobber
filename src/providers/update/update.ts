import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';



import 'rxjs/add/operator/map';
/*
  Generated class for the UpdateProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UpdateProvider {
  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1";

  constructor(private http: Http) {}


  // getUserWorkDetails(userId) {
  //   return this.http.get(`${this.baseUrl}/works/${userId}`);
  // }

    fetchWorkDetails(userId) {

    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        this.http.get(`${this.baseUrl}/works/${userId}`)
          .subscribe(res => {
            console.log(res);
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });

  }

}
