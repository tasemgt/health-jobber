import { Injectable } from "@angular/core";

@Injectable()
export class FirebaseService{

  constructor(){}


//Tranforms a Datasnapshot into a javascript array.
public snapshotToArray(snapshot){
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};


}
