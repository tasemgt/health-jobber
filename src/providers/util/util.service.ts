import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController, Loading } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class UtilService {

  public newSwipeRight: Subject<boolean>;
  public numOfApplications: Subject<number>;

  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toast: ToastController
  ) {
    this.newSwipeRight = new Subject<boolean>();
    this.numOfApplications = new Subject<number>();
  }


  public showAlert(title: string, message: string): void {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ["OK"]
    });
    alert.present();
  }

  presentConfirm(title: string, message: string, buttons: any[]): void {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: buttons
    });
    alert.present();
  }


  public presentLoadingDefault(message: string): Loading {
    let loading = this.loadingCtrl.create({
      content: message
    });

    loading.present();
    return loading;
    // setTimeout(() => {
    //   loading.dismiss();
    // }, 5000);
  }

  public showToast(message: string): void {
    this.toast
      .create({
        message: message,
        duration: 5000
      })
      .present();
  }

  public objectIsEmpty(obj: Object): boolean {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  public refineDate(date:any): string{
    let dateString = `${date.year}-${date.month}-${date.day}`;
    return dateString;
  }

  // public restoreDate(date: string): any{
  //   let dateOb = {
  //     day: Number(date.substring(8)),
  //     month: Number(date.substring(5,7)),
  //     year: Number(date.substring(0,4))
  //   }
  //   console.log(dateOb);
  //   return dateOb;
  // }
}
