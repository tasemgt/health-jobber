
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
/*
  Generated class for the SmartCvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SmartCvProvider {

  constructor(public http: HttpClient) {
    console.log('Hello SmartCvProvider Provider');
  }

  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1";



  generateCv(userId) {
    return this.http.post(`${this.baseUrl}/users/${userId}/smart_cvs`, userId, { observe: 'response'});

  }



}



