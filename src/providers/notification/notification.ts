import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';


@Injectable()
export class NotificationProvider {

  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1";
  private notificationId: number;


  constructor(private http: HttpClient, private localNotifications: LocalNotifications) {
    this.notificationId = 0;
  }


  public getNotifications(userId: number){
    return this.http.get(`${this.baseUrl}/users/${userId}/push_notifications`);
  }

  public updateNotification(userId: number, notificationId: number, payload: any) {
    return this.http.put(`${this.baseUrl}/users/${userId}/push_notifications/${notificationId}`, payload);
  }

  public scheduleLocalNotification(title: string, notification: string, message: string, published: any): number {

    this.notificationId++;

    this.localNotifications.schedule({
      id: this.notificationId,
      title: title,
      text: notification,
      data: { message: message },
      trigger: { at: published },
      foreground: true,
      led: true,
      lockscreen: true,
      smallIcon: 'file://icon/icon.png',
      icon: 'res://icon'
    });

    console.log("From notification service");
    return this.notificationId;
  }
}
