import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WorkCredentials } from "../../models/user";
import { ProfileDetails } from "../../models/user";
import { Storage } from '@ionic/storage';

@Injectable()
export class UserService {

  private baseUrl: string = "https://hjstage.herokuapp.com/api/v1/users";

  constructor(
    private http: HttpClient,
    private storage: Storage
  ) {}

  public getJobSeekers() {
    return this.http.get(`${this.baseUrl}/jobseeker`);
  }

  public getJobApplicationUsers(jobId: number){
    return this.http.get(`https://hjstage.herokuapp.com/api/v1/jobs/${jobId}/users_applied`)
  }

  public getUserById(id: number){
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  public getUsersByRole(role: string){
    return this.http.get(`https://hjstage.herokuapp.com/api/v1/get_users/${role}`);
  }

  public getApprovedUsersByEmployer(empId: number){
    return this.http.get(`${this.baseUrl}/api/v1/users/${empId}/users_approved`);
  }

  public getCurrentUserFromStorage(): Promise<string>{
    return this.storage.get('currentUser');
  }

   //Update Work Details
  public updateWork(workDetails: WorkCredentials) {
      let work = {
        work:  workDetails
      }

     let userid = workDetails.user_id;

     console.log(work);
    return this.http.post(this.baseUrl+'/'+userid+'/works/', work, { observe: 'response' });
  }


     //Update Work Details
  public updateProfile(profileDetails: ProfileDetails) {
      // let profile = {
      //   profile:  profileDetails
      // }

     let userid = profileDetails.user_id;

    return this.http.put(this.baseUrl+'/'+userid, profileDetails, { observe: 'response' });
  }


}
