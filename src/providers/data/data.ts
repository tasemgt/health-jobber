import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataProvider {

  constructor(public http: HttpClient) { }

  /**
   * --------------------------------------------------------
   * Get List of Users
   * --------------------------------------------------------
   */
  getUserList() {
    return Observable.create(observer => {
      return this.http.get('assets/data/data.json').subscribe((result: any) => {
        observer.next(result.Users);
        observer.complete();
      });
    });
  }

  /**
   * --------------------------------------------------------
   * Slider Data
   * --------------------------------------------------------
   */
  profileSlider() {
    return [


      {
        "title": "Get HealthJobber Premium",
        "details": "See jobs you like & more!"
      },
      {
        "title": "Get Matched for Jobs Faster",
        "details": "Let Employers Find You Easily"
      },
      {
        "title": "Control Your Profile",
        "details": "Limit what others see health jobber plus"
      },

      {
        "title": "Smart CVs",
        "details": "Get Custom smart cvs "
       },
       {
        "title": "Increase Your Chances",
        "details": "Get unlimited likes with health jobber plus"
      }

    ]
  }
}
